from credentials import Credentials
from pages.authentication_page import AuthenticationPage
from urls import Urls


class AuthTest:

    def test_auth_invalid_credentials(self, driver):
        auth = AuthenticationPage(driver)
        auth.navigate(Urls.auth_url)
        auth.fill_email(Credentials.login_invalid_email)
        auth.fill_password(Credentials.login_invalid_password)
        auth.sign_in_invalid()
        assert 'Authentication failed' in auth.get_alert_message_text()

    def test_auth_valid_credentials(self, driver):
        auth = AuthenticationPage(driver)
        auth.navigate(Urls.auth_url)
        auth.fill_email(Credentials.login_valid_email)
        auth.fill_password(Credentials.login_valid_password)
        account_pg = auth.sign_in_valid()
        assert account_pg.get_url() == Urls.account_url