import requests
key = ""
token = ""

def create_board(board_name):
    url = "https://api.trello.com/1/boards/"
    querystring = {"Teste Deal": board_name, "": key, "": token}
    response = requests.request("POST", url, params=querystring)
    board_id = response.json()["shortUrl"].split("/")[-1].strip()
    return board_id

def create_list(board_id, list_name):
    url = f"https://api.trello.com/1/boards/{board_id}/lists"
    querystring = {"Backlog": list_name, "": key, "": token}
    response = requests.request("POST", url, params=querystring)
    list_id = response.json()["id"]
    return list_id

def create_list(board_id, list_name):
    url = f"https://api.trello.com/1/boards/{board_id}/lists"
    querystring = {"ToDo": list_name, "": key, "": token}
    response = requests.request("POST", url, params=querystring)
    list_id = response.json()["id"]
    return list_id

def create_list(board_id, list_name):
    url = f"https://api.trello.com/1/boards/{board_id}/lists"
    querystring = {"in Progress": list_name, "": key, "": token}
    response = requests.request("POST", url, params=querystring)
    list_id = response.json()["id"]
    return list_id

def create_list(board_id, list_name):
    url = f"https://api.trello.com/1/boards/{board_id}/lists"
    querystring = {"Testing": list_name, "": key, "": token}
    response = requests.request("POST", url, params=querystring)
    list_id = response.json()["id"]
    return list_id

def create_list(board_id, list_name):
    url = f"https://api.trello.com/1/boards/{board_id}/lists"
    querystring = {"Done": list_name, "": key, "": token}
    response = requests.request("POST", url, params=querystring)
    list_id = response.json()["id"]
    return list_id

def create_card(list_id, card_name):
    url = f"https://api.trello.com/1/cards"
    querystring = {"Test": card_name, "idList": list_id, "": key, "": token}
    response = requests.request("POST", url, params=querystring)
    card_id = response.json()["id"]
    return card_id

def move_trello_card(trello_card_id, list_id):
    url = f"https://api.trello.com/1/cards"
    trello_card = trello_client.get_card(trello_card_id)
    # await my_board.get_list(lambda l: l.name == "My Other List")
	# await card.move_to(my_other_list)

def delete_trello_card(trello_card_id):
    trello_client = TrelloClient(api_key=trello_api_key, api_secret=trello_api_secret, token=trello_token, token_secret=trello_token_secret)
    try:
        trello_card = trello_client.get_card(trello_card_id)
        trello_card.delete()
    except Exception:
        print('Cannot find Trello card with ID {0} deleted in Task Warrior. Maybe you deleted it in Trello too.'.format(trello_card_id))